package net.ciderpunk.swapshop;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SwapShop extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;
  boolean ready = false;



	@Override
	public void create (){
    batch = new SpriteBatch();

    new Thread(new Runnable(){
      @Override
      public void run() {


        Net.HttpRequest httpGet = new Net.HttpRequest(Net.HttpMethods.GET);
        httpGet.setUrl(Constants.RandomUrl);
        Gdx.net.sendHttpRequest(httpGet, new Net.HttpResponseListener(){
          @Override
          public void handleHttpResponse(Net.HttpResponse httpResponse) {
            byte[] data = httpResponse.getResult();
            final Pixmap pm = new Pixmap(data, 0, data.length);

            Gdx.app.postRunnable(new Runnable() {
              @Override
              public void run () {
                img = new Texture(pm);
              }
            });

            //pm.dispose();
            ready = true;
          }
          @Override
          public void failed(Throwable t) {

          }
          @Override
          public void cancelled() {

          }

        });


      }
    }).start();

		//img = new Texture("badlogic.jpg");
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    if (ready) {
      batch.begin();
      batch.draw(img, 0, 0);
      batch.end();
    }
	}
}
