package net.ciderpunk.swapshop.terrain;

import com.badlogic.gdx.utils.ObjectMap;
import net.ciderpunk.gamebase.utils.Vector2i;

/**
 * Created by matthewlander on 16/05/15.
 */
public class Map {

	ObjectMap<Vector2i, Chunk> chunks;


	public Chunk getChunk(Vector2i coord){
		Chunk chunk = chunks.get(coord);
		if (chunk == null){
			chunk = new Chunk(coord);
			chunks.put(coord, chunk);
		}
		return chunk;
	}



}
